# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cart',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('payment_type', models.CharField(max_length=100, null=True)),
                ('total', models.PositiveIntegerField(null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Producto_Cart',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cantidad', models.PositiveIntegerField()),
                ('cart', models.ForeignKey(to='app.Cart')),
            ],
        ),
        migrations.AddField(
            model_name='producto',
            name='imagen',
            field=models.ImageField(default=b'pic_folder/None/no-img.jpg', upload_to=b'img/'),
        ),
        migrations.AddField(
            model_name='producto_cart',
            name='producto',
            field=models.ForeignKey(to='app.Producto'),
        ),
    ]
