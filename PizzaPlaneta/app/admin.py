from django.contrib import admin
from .models import Usuario,Producto,Proveedor,Categoria,Pedido,Cart,Producto_Cart
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
# Register your models here.
admin.site.register(Usuario)
admin.site.register(Producto)
admin.site.register(Proveedor)
admin.site.register(Categoria)
admin.site.register(Pedido)
admin.site.register(Cart)
admin.site.register(Producto_Cart)
