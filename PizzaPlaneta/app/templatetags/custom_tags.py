from django import template
register = template.Library()

from app.models import Categoria,Producto,Proveedor,Cart

@register.simple_tag
def sumar(cantidad,price):
    variable = cantidad * price
    return variable

@register.inclusion_tag('sumar.html')
def lista_productos():
    # variable = Producto.objects.all()[:3]
    last_ten = Producto.objects.all().order_by('-id')[:3]
    last_ten_in_ascending_order = reversed(last_ten)
    return {'variable': last_ten_in_ascending_order}


@register.inclusion_tag('prov_tag.html')
def lista_proveedores():
    # variable = Producto.objects.all()[:3]
    last_ten = Proveedor.objects.all().order_by('-id')[:3]
    last_ten_in_ascending_order = reversed(last_ten)
    return {'variable': last_ten_in_ascending_order}

@register.inclusion_tag('cat_tag.html')
def lista_categorias():
    # variable = Producto.objects.all()[:3]
    last_ten = Categoria.objects.all().order_by('-id')[:3]
    last_ten_in_ascending_order = reversed(last_ten)
    return {'variable': last_ten_in_ascending_order}

@register.inclusion_tag('ped_tag.html')
def lista_pedidos():
    # variable = Producto.objects.all()[:3]
    last_ten = Cart.objects.filter(active = False).order_by('-id')[:3]
    last_ten_in_ascending_order = reversed(last_ten)
    return {'variable': last_ten_in_ascending_order}
