from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.views import login, logout_then_login ,logout
from app import views
from django.conf.urls.static import url, static
from django.conf import settings
urlpatterns = [
    # Examples:
    # url(r'^$', 'PizzaPlaneta.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.Index , name='index'),
    url(r'^about_us/$', views.About_Us , name='about_us'),
    url(r'^productos$', views.Productos , name='productos'),
    url(r'^pastas$', views.Pastas , name='pastas'),
    url(r'^Postres$', views.Postres , name='Postres'),
    url(r'^wsProductos/$',views.wsProductos, name="wsProductos_view"),
    url(r'^bebidas$', views.Bebidas, name='bebidas'),
    #url(r'^producto_detalle$', views.Producto_detalle, name='Producto_detalle'),
    url(r'^producto/(?P<productoId>\d+)/$', views.Producto_detalle, name="Producto_detalle"),
    url(r'^promociones$', views.Promociones, name='promociones'),
    url(r'^signup$', views.Signup.as_view(), name='signup_view'),
    url(r'^login$',views.login_view,name="login"),
    url(r'^logout$',views.logout_view,name="logout"),

    url(r'^crearProd$',views.ProductoCreate.as_view(),name="producto_create"),
    url(r'^crearProv$',views.ProveedorCreate.as_view(),name="proveedor_create"),
    url(r'^crearCat$',views.CategoriaCreate.as_view(),name="categoria_create"),

    url(r'^update$',views.All_Productos,name="update"),
    url(r'^updateP$',views.All_Proveedores,name="updateP"),
    url(r'^updateC$',views.All_Categorias,name="updateC"),
    url(r'^updatePe$',views.All_Pedidos,name="updatePe"),
    # +++
    url(r'^update_producto/(?P<pk>\d+)/$',views.Producto_Update.as_view(),name="update_producto"),
    url(r'^update_proveedor/(?P<pk>\d+)/$',views.Proveedor_Update.as_view(),name="update_proveedor"),
    url(r'^update_categoria/(?P<pk>\d+)/$',views.Categoria_Update.as_view(),name="update_categoria"),
    url(r'^detail_pedido/(?P<pk>\d+)/$',views.Detail_Cart.as_view(),name="detail_pedido"),

    url(r'^update_producto/$',views.BuscarView.as_view(),name="buscarProducto_view"),
    url(r'^update_categoria/$',views.BuscarViewC.as_view(),name="buscarCategoria_view"),
    url(r'^update_proveedor/$',views.BuscarViewP.as_view(),name="buscarProveedor_view"),
    url(r'^update_pedido/$',views.BuscarViewPe.as_view(),name="buscarPedido_view"),

    url(r'^delete_producto/(?P<pk>\d+)/$',views.ProductoDelete.as_view(),name="delete_producto"),
    url(r'^delete_proveedor/(?P<pk>\d+)/$',views.ProveedorDelete.as_view(),name="delete_proveedor"),
    url(r'^delete_categoria/(?P<pk>\d+)/$',views.CategoriaDelete.as_view(),name="delete_categoria"),

    url(r'^checkout/',views.checkout,name='checkout'),
    url(r'^cart$',views.cart,name="cart"),
    url(r'^add_to_cart/(\d+)/$',views.add_to_cart,name="add_to_cart"),
    url(r'^clean_cart$',views.clean_cart,name="clean_cart"),
    url(r'^remove_from_cart/(\d+)/$',views.remove_from_cart,name="remove_from_cart"),
    # url(r'^sig$', views.STest, name='signup'),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
