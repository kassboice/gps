from django.contrib.auth.forms import UserCreationForm
from django import forms
from .models import Usuario,Producto,Proveedor,Categoria
from django.contrib.auth.models import User

class Usuario_Form(UserCreationForm):

    username = forms.RegexField(regex=r'^[\w.@+-]+$',
        help_text = (""),
        label=("Email:"),
        error_messages = {'invalid': ("Nel Prro No es Cierto. No seas verbo")})
    password1 = forms.CharField(label=("Contrasena:"),widget=forms.PasswordInput)
    password2 = forms.CharField(label=("Repetir Contrasena:"),widget=forms.PasswordInput)
    first_name = forms.CharField(label=("Nombre:"),max_length=40)
    last_name = forms.CharField(label=("Apellido:"),max_length=40)
    edad = forms.IntegerField(label=("Edad:"))
    direccion = forms.CharField(label=("Direccion:"),max_length=40)
    telefono = forms.IntegerField(label=("Telefono:"))

    class Meta:
        model = User
        fields = ['username','password1','password2','first_name','last_name']

    """
    def clean_username(self):
        data = self.cleaned_data['username']

        if data == "Juana":
            raise forms.ValidationError("Error, Juana No Es")
        if not data:
            raise forms.ValidationError("No hay username")
        else:
            raise forms.ValidationError("Si hay username")
        return data
    """

class Producto_Form(forms.Form):
    nombre = forms.CharField(max_length=30)
    descripcion = forms.CharField(widget=forms.Textarea)
    categoria = forms.ModelChoiceField(queryset = Categoria.objects.all())
    precio = forms.IntegerField()
    proveedor = forms.ModelChoiceField(queryset = Proveedor.objects.all())
class Proveedor_Form(forms.Form):
    nombre = forms.CharField(max_length=30)
    direccion = forms.CharField(max_length=30)
    telefono = forms.IntegerField()
class Categoria_Form(forms.Form):
    tipo = forms.CharField(max_length=30)
