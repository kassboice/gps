from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Proveedor(models.Model):
    nombre = models.CharField(max_length=35)
    direccion = models.CharField(max_length=50)
    telefono = models.IntegerField()
    def __unicode__(self):
        return self.nombre

class Categoria(models.Model):
    tipo = models.CharField(max_length=35)
    def __unicode__(self):
        return self.tipo

class Producto(models.Model):
    nombre = models.CharField(max_length=35)
    descripcion = models.TextField()
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    precio = models.IntegerField()
    proveedor = models.ForeignKey(Proveedor, on_delete=models.CASCADE)
    imagen = models.ImageField(upload_to = 'img/', default = 'pic_folder/None/no-img.jpg')
    def __unicode__(self):
        return self.nombre

class Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    active = models.BooleanField(default=True)
    order_date = models.DateField(null = True)
    payment_type = models.CharField(max_length=100,null=True)
    total = models.PositiveIntegerField(null=True)
    def add_to_cart(self,producto_id):
		producto = Producto.objects.get(pk = producto_id)
		try:
			preexisting_order = Producto_Cart.objects.get(producto = producto, cart = self)
			preexisting_order.cantidad +=1
			preexisting_order.save()
		except Producto_Cart.DoesNotExist:
			new_order = Producto_Cart.objects.create(
				producto = producto,
				cart = self,
				cantidad = 1,
				)
			new_order.save()
    def remove_from_cart(self, producto_id):
		producto = Producto.objects.get(pk = producto_id)
		try:
			preexisting_order = Producto_Cart.objects.get(producto = producto, cart=self)
			if preexisting_order.cantidad > 1:
				preexisting_order.cantidad -=1
				preexisting_order.save()
			else:
				preexisting_order.delete()
		except Producto_Cart.DoesNotExist:
			pass
    def destroy_from_cart(self, producto_id):
        producto = Producto.objects.get(pk = producto_id)
        try:
            preexisting_order = Producto_Cart.objects.get(producto = producto, cart=self)
            preexisting_order.delete()
        except Producto_Cart.DoesNotExist:
            pass

    def __unicode__(self):
		return self.user.username

class Producto_Cart(models.Model):
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    cart = models.ForeignKey(Cart, on_delete = models.CASCADE)
    cantidad = models.PositiveIntegerField()
    def __unicode__(self):
		return self.producto.nombre

class Usuario(models.Model):
    nombre = models.OneToOneField(User)
    edad = models.IntegerField()
    direccion = models.CharField(max_length=50)
    telefono = models.IntegerField()
    def __unicode__(self):
        return self.nombre.username

class Pedido(models.Model):
    num_pedido = models.PositiveIntegerField(unique=True)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    producto = models.ManyToManyField(Producto,null=True)
    estado = models.PositiveIntegerField()
    def __unicode__(self):
        return "Pedido:" + str(self.num_pedido)
"""
class Usuario_Model(models.Model):
    edad = models.IntegerField()
    direccion = models.CharField(max_length=50)
    telefono = models.IntegerField()
    def __unicode__(self):
        return self.edad

class UserProfile(User):
    usuario_p = models.ForeignKey(Usuario_Model, on_delete=models.CASCADE,verbose_name="Usuario")
    def __unicode__(self):
        return self.username
"""
