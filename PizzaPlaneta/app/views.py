from django.shortcuts import render
from .models import Usuario,Producto,Proveedor,Categoria,Cart,Producto_Cart
from django.contrib.auth.models import User
from django.views.generic.edit import FormView,CreateView,UpdateView,DeleteView
from .forms import Producto_Form,Usuario_Form,Proveedor_Form,Categoria_Form
from django.core.urlresolvers import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic import TemplateView
from django.http import HttpResponse,HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import redirect
from django.utils import timezone
from datetime import datetime, timedelta
from django.core import serializers


# Create your views here.
def wsProductos(request):
    data = serializers.serialize('json',Producto.objects.all()[:3])
    print(type(data))
    return HttpResponse(data,content_type='application/json')

def STest(request):
    return render(request,"TestSign.html")
def About_Us(request):
    return render(request,"aboutus.html")
def Index(request):
    return render(request,"index.html")
def Productos(request):
    pizzas = Producto.objects.filter(categoria__tipo='Pizzas')
    return render(request,"test.html",{'pizzas':pizzas})
def Pastas(request):
    pastas = Producto.objects.filter(categoria__tipo='Pastas')
    return render(request,"pastas.html",{'pastas':pastas})
def Postres(request):
    postres = Producto.objects.filter(categoria__tipo='Postres')
    return render(request,"Postres.html",{'postres':postres})
def Bebidas(request):
    bebidas = Producto.objects.filter(categoria__tipo='Bebidas')
    return render(request,"bebidas.html",{'bebidas':bebidas})
def Promociones(request):
    return render(request,"promociones.html")
def Producto_detalle(request,productoId):
    producto = Producto.objects.get(id = productoId)
    return render(request,"DetalleProducto.html",{'producto':producto})


class Detail_Cart(DetailView):
    model = Cart
    template_name = "cart_detail.html"
    def get_context_data(self, **kwargs):
        context = super(Detail_Cart, self).get_context_data(**kwargs)
        # print(type(self.request.user))
        p = Producto_Cart.objects.all().filter(cart = context['object'])
        context['productos'] = list(p)
        return context

def checkout(request):
    if request.user.is_authenticated():
        cart = Cart.objects.filter(user=request.user, active=True)
        orders = Producto_Cart.objects.filter(cart=cart)
        total = 0
        for order in orders:
			total += ((order.producto.precio) * order.cantidad)
        cart = Cart.objects.get(user=request.user, active=True)
        cart.active = False
        cart.order_date = timezone.now()
        cart.total = total
        cart.save()
        return redirect('index')
    else:
        return redirect('index')

class ProductoCreate(CreateView):
    model = Producto
    fields = '__all__'
    template_name = "producto_create.html"
    success_url = reverse_lazy('producto_create')
class ProveedorCreate(CreateView):
    model = Proveedor
    fields = '__all__'
    template_name = "proveedor_create.html"
    success_url = reverse_lazy('proveedor_create')
class CategoriaCreate(CreateView):
    model = Categoria
    fields = '__all__'
    template_name = "categoria_create.html"
    success_url = reverse_lazy('categoria_create')

def GetTime(s):
    sec = timedelta(seconds = int(s))
    d = datetime(1,1,1) + sec
    return d
    # print("DAYS:HOURS:MIN:SEC")
    # print("%d:%d:%d:%d" % (d.day-1, d.hour, d.minute, d.second))

def cart(request):
    if request.user.is_authenticated():
        try:
            cart = Cart.objects.get(user = request.user, active=True)
        except ObjectDoesNotExist:
            cart = Cart.objects.create(user = request.user)
            cart.save()
            return redirect('cart')
        orders = Producto_Cart.objects.filter(cart = cart)
        total = 0
        count = 0
        tiempo = 0
        for order in orders:
            total +=((order.producto.precio) * order.cantidad)
            count += order.cantidad
        for order in orders:
            if order.producto.categoria.tipo != "Bebidas":
                tiempo += order.cantidad * 1200
                print("Paso")
        # tiempo = tiempo / 60
        # print("+",tiempo)
        mtime = GetTime(tiempo)
        print(mtime)
        if len(orders) > 0:
            iempty = False
        else:
            iempty = True
        # abc = Cart.objects.filter(user = request.user, active = False)
        context = {
        'cart': orders,
        'total':total,
        'count': count,
        'carrito':cart,
        'iempty':iempty,
        'mtime':mtime
        }
        return render(request, 'cart.html', context)
    else:
        return redirect('index')

def add_to_cart(request,producto_id):
	if request.user.is_authenticated():
		try:
			producto = Producto.objects.get(pk = producto_id)
		except ObjectDoesNotExist:
			pass
		else:
			try:
				cart = Cart.objects.get(user=request.user, active=True)
			except ObjectDoesNotExist:
				cart = Cart.objects.create( user=request.user)
				cart.save()
			cart.add_to_cart(producto_id)
		return redirect('cart')
	else:
		return redirect('index')
def remove_from_cart(request,producto_id):
	if request.user.is_authenticated():
		try:
			producto = Producto.objects.get(pk=producto_id)
		except ObjectDoesNotExist:
			pass
		else:
			cart = Cart.objects.get(user = request.user, active=True)
			cart.remove_from_cart(producto_id)
		return redirect('cart')
	return redirect('index')

def clean_cart(request):
    if request.user.is_authenticated():
        cart = Cart.objects.get(user = request.user, active=True)
        # print("*")
        # print(cart)
        # print("*")
        orders = Producto_Cart.objects.filter(cart = cart)
        # print(len(orders))
        for order in orders:
            cart.destroy_from_cart(order.producto_id)
        return redirect('cart')
    else:
        return redirect('index')

def All_Productos(request):
    if request.user.is_authenticated():
        if request.user.is_superuser:
            return render(request,"list_producto.html")
        else:
            return HttpResponseRedirect(reverse_lazy('index'))
    else:
        return HttpResponseRedirect(reverse_lazy('index'))

def All_Proveedores(request):
    if request.user.is_authenticated():
        if request.user.is_superuser:
            return render(request,"list_proveedor.html")
        else:
            return HttpResponseRedirect(reverse_lazy('index'))
    else:
        return HttpResponseRedirect(reverse_lazy('index'))

def All_Categorias(request):
    if request.user.is_authenticated():
        if request.user.is_superuser:
            return render(request,"list_categorias.html")
        else:
            return HttpResponseRedirect(reverse_lazy('index'))
    else:
        return HttpResponseRedirect(reverse_lazy('index'))

def All_Pedidos(request):
    if request.user.is_authenticated():
        if request.user.is_superuser:
            return render(request,"list_pedidos.html")
        else:
            return HttpResponseRedirect(reverse_lazy('index'))
    else:
        return HttpResponseRedirect(reverse_lazy('index'))

class BuscarView(TemplateView):
    template_name = "update_producto.html"
    def post(self,request,*args,**kwargs):
        buscar = request.POST['buscalo']
        print buscar
        # __contains
        productos = Producto.objects.filter(nombre__contains = buscar)
        print productos
        ctx = {
        'items':productos
        }
        return render(request,"update_producto.html",ctx)

class BuscarViewP(TemplateView):
    template_name = "update_proveedor.html"
    def post(self,request,*args,**kwargs):
        buscar = request.POST['buscalo']
        print buscar
        # __contains
        proveedores = Proveedor.objects.filter(nombre__contains = buscar)
        # print productos
        ctx = {
        'items':proveedores
        }
        return render(request,"update_proveedor.html",ctx)
class BuscarViewC(TemplateView):
    template_name = "update_categoria.html"
    def post(self,request,*args,**kwargs):
        buscar = request.POST['buscalo']
        print buscar
        # __contains
        categorias = Categoria.objects.filter(tipo__contains = buscar)
        # print productos
        ctx = {
        'items':categorias
        }
        return render(request,"update_categoria.html",ctx)

class BuscarViewPe(TemplateView):
    template_name = "update_pedido.html"
    def post(self,request,*args,**kwargs):
        buscar = request.POST['buscalo']
        print buscar
        # __contains
        pedidos = Cart.objects.filter(order_date__contains = buscar, active = False)
        # print productos
        ctx = {
        'items':pedidos
        }
        return render(request,"update_pedido.html",ctx)

class Producto_Update(UpdateView):
    template_name = "modificar_producto.html"
    model = Producto
    fields = '__all__'
    # fields = ['matricula','first_name' ,'last_name','birth_date','email','address','carrera','phone','curp','vigencia','modulo']
    # exclude = ('nombre',)
    success_url = reverse_lazy("index")
    def get_context_data(self, **kwargs):
        context = super(Producto_Update, self).get_context_data(**kwargs)
        # Refresh the object from the database in case the form validation changed it
        object = self.get_object()
        context['object'] = context['Producto'] = object
        return context

class Proveedor_Update(UpdateView):
    template_name = "modificar_proveedor.html"
    model = Proveedor
    fields = '__all__'
    # fields = ['matricula','first_name' ,'last_name','birth_date','email','address','carrera','phone','curp','vigencia','modulo']
    # exclude = ('nombre',)
    success_url = reverse_lazy("index")
    def get_context_data(self, **kwargs):
        context = super(Proveedor_Update, self).get_context_data(**kwargs)
        # Refresh the object from the database in case the form validation changed it
        object = self.get_object()
        context['object'] = context['Proveedor'] = object
        return context

class Categoria_Update(UpdateView):
    template_name = "modificar_categoria.html"
    model = Categoria
    fields = '__all__'
    # fields = ['matricula','first_name' ,'last_name','birth_date','email','address','carrera','phone','curp','vigencia','modulo']
    # exclude = ('nombre',)
    success_url = reverse_lazy("index")
    def get_context_data(self, **kwargs):
        context = super(Categoria_Update, self).get_context_data(**kwargs)
        # Refresh the object from the database in case the form validation changed it
        object = self.get_object()
        context['object'] = context['Categoria'] = object
        return context

class ProductoDelete(DeleteView):
    model = Producto
    template_name = "borrar_producto.html"
    success_url = reverse_lazy('index')

class ProveedorDelete(DeleteView):
    model = Proveedor
    template_name = "borrar_proveedor.html"
    success_url = reverse_lazy('index')

class CategoriaDelete(DeleteView):
    model = Categoria
    template_name = "borrar_categoria.html"
    success_url = reverse_lazy('index')


def login_view(request):
    u = request.POST.get('username')
    p = request.POST.get('password')

    try:
        user = authenticate(username=u, password=p)
    except:
        print "*Salio Error*"
    else:
        print "*Todo bien*"
    finally:
        print user
    # Loguear
    if user is not None:
        login(request, user)
        return HttpResponseRedirect(reverse_lazy('index'))
    else:
        return render(request,"Login.html")

def logout_view(request):
    logout(request)
    return render(request,"index.html")

class Signup(FormView):
    template_name = 'Signup.html'
    form_class = Usuario_Form
    success_url = reverse_lazy('index')
    def form_valid(self,form):
        user = form.save()
        p = Usuario()
        p.nombre = user
        p.edad = form.cleaned_data['edad']
        p.direccion = form.cleaned_data['direccion']
        p.telefono = form.cleaned_data['telefono']
        p.save()
        return super(Signup,self).form_valid(form)
def Producto_View(request):
    obj = Producto()
    form = Producto_Form(request.POST or None)
    if request.method=='POST':
        if form.is_valid():
            nombre = form.cleaned_data["nombre"]
            descripcion = form.cleaned_data["descripcion"]
            precio = form.cleaned_data["precio"]
            categoria = form.cleaned_data["categoria"]
            proveedor = form.cleaned_data["proveedor"]

            obj.nombre = nombre
            obj.descripcion = descripcion
            obj.precio = precio
            obj.categoria = categoria
            obj.proveedor = proveedor
            obj.save()
            return HttpResponseRedirect(reverse_lazy('login'))
    ctx = {
    'form':form
    }
    return render(request,"register_producto.html",ctx)
